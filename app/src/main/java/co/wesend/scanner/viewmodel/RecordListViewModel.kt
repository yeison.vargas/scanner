package co.wesend.scanner.viewmodel

import androidx.lifecycle.*
import co.wesend.scanner.db.entity.Record
import co.wesend.scanner.repository.RecordRepository
import co.wesend.scanner.vo.Resource
import co.wesend.scanner.vo.Status
import javax.inject.Inject


class RecordListViewModel @Inject constructor (
    private val recordRepository: RecordRepository
) : ViewModel() {

    val recordList = MediatorLiveData<List<Record>>()

    init {
        recordList.addSource(recordRepository.getAllRecords(), recordList::setValue)
    }

    fun getAllRecords() = recordList


}
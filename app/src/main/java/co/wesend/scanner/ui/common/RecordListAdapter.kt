package co.wesend.scanner.ui.common


import androidx.databinding.DataBindingComponent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import co.wesend.scanner.AppExecutors
import co.wesend.scanner.R
import co.wesend.scanner.databinding.RecordItemHomeBinding
import co.wesend.scanner.db.entity.Record


/**
 * A RecyclerView adapter for [Record] class.
 */
class RecordListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors
) : DataBoundListAdapter<Record, RecordItemHomeBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Record>() {
        override fun areItemsTheSame(oldItem: Record, newItem: Record): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Record, newItem: Record): Boolean {
            return oldItem.rate == newItem.rate
                    && oldItem.numOutliers == newItem.numOutliers
        }
    }
) {

    override fun createBinding(parent: ViewGroup): RecordItemHomeBinding {
        val binding = DataBindingUtil.inflate<RecordItemHomeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.record_item_home,
            parent,
            false,
            dataBindingComponent
        )
        return binding
    }

    override fun bind(binding: RecordItemHomeBinding, item: Record) {
        binding.record = item
    }
}
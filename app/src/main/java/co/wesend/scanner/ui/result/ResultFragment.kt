package co.wesend.scanner.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.wesend.scanner.AppExecutors
import co.wesend.scanner.R
import co.wesend.scanner.binding.FragmentDataBindingComponent
import co.wesend.scanner.databinding.ResultFragmentBinding
import co.wesend.scanner.di.Injectable
import co.wesend.scanner.util.autoCleared
import javax.inject.Inject

class ResultFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<ResultFragmentBinding>()

    lateinit var resultViewModel: ResultViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.result_fragment,
            container,
            false,
            dataBindingComponent
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        resultViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(ResultViewModel::class.java)
        binding.setLifecycleOwner(viewLifecycleOwner)
        bindRecord()
    }

    private fun bindRecord() {
        resultViewModel.record.observe(viewLifecycleOwner, Observer { record ->
            binding.record = record
        })
    }
}

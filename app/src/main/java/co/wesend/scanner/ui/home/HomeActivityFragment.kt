package co.wesend.scanner.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.wesend.scanner.AppExecutors
import co.wesend.scanner.R
import co.wesend.scanner.binding.FragmentDataBindingComponent
import co.wesend.scanner.databinding.FragmentHomeBinding
import co.wesend.scanner.di.Injectable
import co.wesend.scanner.ui.common.RecordListAdapter
import co.wesend.scanner.util.autoCleared
import co.wesend.scanner.viewmodel.RecordListViewModel
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class HomeActivityFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<FragmentHomeBinding>()
    var adapter by autoCleared<RecordListAdapter>()

    lateinit var recordListViewModel: RecordListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false,
            dataBindingComponent
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recordListViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(RecordListViewModel::class.java)
        binding.setLifecycleOwner(viewLifecycleOwner)


        val rvAdapter = RecordListAdapter(
            dataBindingComponent = dataBindingComponent,
            appExecutors = appExecutors
        )

        adapter = rvAdapter
        binding.recordsList.adapter = adapter

        initRecyclerView()
    }

    private fun initRecyclerView() {

        recordListViewModel.recordList.observe(viewLifecycleOwner, Observer { result ->
            adapter.submitList(result.sortedByDescending { it.dateTime })
        })
    }
}

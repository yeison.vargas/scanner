package co.wesend.scanner.di


import co.wesend.scanner.ui.home.HomeActivityFragment
import co.wesend.scanner.ui.result.ResultFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Qualifier

@Module
abstract class FragmentBuildersModule {

   @ContributesAndroidInjector
   abstract fun contributeRecordFragment(): HomeActivityFragment

   @ContributesAndroidInjector
   abstract fun contributeResultFragment(): ResultFragment
}
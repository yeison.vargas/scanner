package co.wesend.scanner.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

/**
 * Binding adapters that work with a fragment instance.
 */
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    @BindingAdapter(value = ["formatSeconds", "pattern"])
    fun secondsToDateText(textView: TextView, dateUTC: Long, pattern: String) {

        val localDateTime = LocalDateTime.ofInstant(
            Instant.ofEpochSecond(dateUTC / 1000),
            ZoneId.systemDefault())

        val formatter = DateTimeFormatter.ofPattern(pattern)
        textView.text = formatter.format(localDateTime)

    }
}

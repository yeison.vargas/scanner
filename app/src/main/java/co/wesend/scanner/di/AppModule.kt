package co.wesend.scanner.di

import android.app.Application
import androidx.room.Room
import co.wesend.scanner.api.LocalBitcoinsService
import co.wesend.scanner.db.AppDatabase
import co.wesend.scanner.db.dao.AdDao
import co.wesend.scanner.db.dao.RecordDao
import co.wesend.scanner.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideLocalBitcoinService(): LocalBitcoinsService {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor(logging)

        return Retrofit.Builder()
            .baseUrl("https://localbitcoins.com")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(httpClient.build())
            .build()
            .create(LocalBitcoinsService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, "scanner.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideAdDao(db: AppDatabase): AdDao {
        return db.adDao()
    }

    @Singleton
    @Provides
    fun provideRecordDao(db: AppDatabase): RecordDao {
        return db.recordDao()
    }

}
package co.wesend.scanner.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "ads")
data class Ad (
    @PrimaryKey @ColumnInfo(name = "id")
    @SerializedName("ad_id")
    val id: String,
    @SerializedName("temp_price")
    val tempPrice: Double,
    @SerializedName("temp_price_usd")
    val tempPriceUsd: Double,
    val currency: String,
    @SerializedName("countrycode")
    val countryCode: String,
    @SerializedName("location_string")
    val locationString: String
) {

    override fun toString() = tempPriceUsd.toString()
}
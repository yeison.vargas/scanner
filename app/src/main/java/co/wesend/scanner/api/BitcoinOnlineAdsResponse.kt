package co.wesend.scanner.api

import co.wesend.scanner.db.entity.Ad
import com.google.gson.annotations.SerializedName


data class Pagination(
    public var next: String? = "",
    public var prev: String? = ""
)

data class ItemAd(
    @SerializedName("data")
    val ad: Ad
)

data class Data(
    @SerializedName("ad_count")
    val total: Int = 0,

    @SerializedName("ad_list")
    val listContent: List<ItemAd>
)

data class BitcoinOnlineAdsResponse(
    val data: Data,
    var pagination: Pagination
)
package co.wesend.scanner.ui.result

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import co.wesend.scanner.db.entity.Record
import co.wesend.scanner.repository.Adrepository
import co.wesend.scanner.repository.RecordRepository
import java.util.*
import javax.inject.Inject
import kotlin.math.ceil


class ResultViewModel @Inject constructor (
    private val recordRepository: RecordRepository,
    private val adRepository: Adrepository
) : ViewModel() {

    val record = MediatorLiveData<Record>()

    init {
        record.addSource(adRepository.loadAds()) {
            if (it.data != null && !it.data.isEmpty()) {

                    val pricesWithOutliers = it.data.map { ad -> ad.tempPriceUsd }

                    val cleanedPrices = removeOutliers(unorderedItems = pricesWithOutliers)

                    val meanRate = cleanedPrices.sum() / it.data.size

                    val numOutliers = pricesWithOutliers.size - cleanedPrices.size

                    val dateTime = Date().time

                    val obj: Record = recordRepository.registerNewPrice(
                        rate = meanRate, numOutliers = numOutliers,
                        dateTime = dateTime
                    )
                    record.value = obj
            }
        }
    }

    // TODO: Move all this business logic to an Interactor
    private fun getMedian(sortedItems: List<Double>): Double {

        if (sortedItems.isEmpty()) {
            throw IllegalArgumentException("items is empty")
        }

        return sortedItems.let { (it[it.size / 2] + it[(it.size - 1) / 2]) / 2 }
    }

    private fun getPercentile(num: Int, sortedItems: List<Double>): Double {

        if (num <= 0 || num > 100) {
            throw IllegalArgumentException("Percentile number must be greater than 0 and less than or equal to 100")
        }

        if (sortedItems.isEmpty()) {
            throw IllegalArgumentException("items is empty")
        }

        return sortedItems[ceil(sortedItems.size * (num / 100.0)).toInt() - 1]
    }

    // Todo: Check performance of this solution
    private fun removeOutliers(unorderedItems: List<Double>): List<Double>{

        var sortedItems = unorderedItems.sorted()

        var wasRemovedOutlier = false
        var indexOutlier: Int

        do {

            val q1 = getPercentile(25, sortedItems)
            val q3 = getPercentile(75, sortedItems)

            val iqr = q3 - q1

            val cutOff = 1.5 * iqr

            val min = q1 - cutOff
            val max = q3 + cutOff

            indexOutlier = sortedItems.indexOfFirst { value -> value < min || value > max }

            if (indexOutlier == -1) {

                wasRemovedOutlier = false

            } else {
                sortedItems = sortedItems.filterIndexed { index, _ -> index != indexOutlier }

                wasRemovedOutlier = true
            }

        } while (wasRemovedOutlier)

        return sortedItems
    }

}

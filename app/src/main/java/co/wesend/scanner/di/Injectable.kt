package co.wesend.scanner.di


/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
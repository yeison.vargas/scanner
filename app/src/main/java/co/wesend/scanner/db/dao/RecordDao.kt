package co.wesend.scanner.db.dao

import androidx.room.Dao
import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.wesend.scanner.db.entity.Record


/**
 * The Data Access Object for the Ad class.
 */
@Dao
interface RecordDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(record: Record): Long

    @Query("SELECT * FROM records")
    fun findAll(): LiveData<List<Record>>

    @Query("SELECT * FROM records WHERE `id` = :id")
    fun getRecord(id: Long): LiveData<Record>

}
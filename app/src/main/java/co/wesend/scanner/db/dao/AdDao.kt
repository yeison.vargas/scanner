package co.wesend.scanner.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.wesend.scanner.db.entity.Ad


/**
 * The Data Access Object for the Ad class.
 */
@Dao
abstract class AdDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(ad: Ad) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAds(repositories: List<Ad>)

    @Query("SELECT * FROM ads")
    abstract fun findAll(): LiveData<List<Ad>>
}
package co.wesend.scanner.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import co.wesend.scanner.AppExecutors
import co.wesend.scanner.api.BitcoinOnlineAdsResponse
import co.wesend.scanner.api.LocalBitcoinsService
import co.wesend.scanner.db.AppDatabase
import co.wesend.scanner.db.dao.AdDao
import co.wesend.scanner.db.entity.Ad
import co.wesend.scanner.repository.NetworkBoundResource
import co.wesend.scanner.vo.Resource
import javax.inject.Inject


class Adrepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val db: AppDatabase,
    private val localBitcoinsService: LocalBitcoinsService,
    private val adDao: AdDao
) {


    fun getAllAds(): LiveData<List<Ad>> {
        return adDao.findAll()
    }

    fun loadAds(): LiveData<Resource<List<Ad>>> {
        return object : NetworkBoundResource<List<Ad>, BitcoinOnlineAdsResponse>(appExecutors) {
            override fun saveCallResult(item: BitcoinOnlineAdsResponse) {
                adDao.insertAds(item.data.listContent.map { it.ad })
            }

            override fun shouldFetch(data: List<Ad>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDb() = adDao.findAll()

            override fun createCall() = localBitcoinsService.getBitcoinsOnlineAds(currency = "VES",
                page = this.nextPage ?: 1)

        }.asLiveData()
    }
}
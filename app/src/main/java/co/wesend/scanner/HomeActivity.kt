package co.wesend.scanner

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.wesend.scanner.ui.result.ResultFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector

import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject
import androidx.navigation.fragment.NavHostFragment
import co.wesend.scanner.ui.home.HomeActivityFragment
import com.jakewharton.threetenabp.AndroidThreeTen


class HomeActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    lateinit var resultFragment: ResultFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidThreeTen.init(this)

        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->

            val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

            val currentFrag = (navHostFragment!!.childFragmentManager.fragments.first())

            if (currentFrag is HomeActivityFragment) {
                findNavController(R.id.nav_host_fragment).navigate(R.id.getPrice)

                fab.setImageResource(R.drawable.ic_close)
            } else if (currentFrag is ResultFragment) {
                findNavController(R.id.nav_host_fragment).navigate(R.id.showHome)
                fab.setImageResource(R.drawable.ic_rate)
            }
        }
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector


}

package co.wesend.scanner.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "records")
data class Record (
    val rate: Double,
    val dateTime: Long,
    val numOutliers: Int
) {
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0
    override fun toString() = "$rate $dateTime"
}
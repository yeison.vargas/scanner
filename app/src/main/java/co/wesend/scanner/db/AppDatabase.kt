package co.wesend.scanner.db

import androidx.room.Database
import androidx.room.RoomDatabase
import co.wesend.scanner.db.dao.AdDao
import co.wesend.scanner.db.dao.RecordDao
import co.wesend.scanner.db.entity.Ad
import co.wesend.scanner.db.entity.Record


/**
 * The Room database for this app
 */
@Database(entities = [Ad::class, Record::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun adDao(): AdDao
    abstract fun recordDao(): RecordDao

}
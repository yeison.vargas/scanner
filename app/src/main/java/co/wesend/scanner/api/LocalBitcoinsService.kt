package co.wesend.scanner.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


const val DEFAULT_FORMAT = ".json"

/**
 * REST API access points
 */
interface LocalBitcoinsService {

    @GET("buy-bitcoins-online/{currency}/$DEFAULT_FORMAT")
    fun getBitcoinsOnlineAds(@Path("currency") currency: String, @Query("page") page: Int):
            LiveData<ApiResponse<BitcoinOnlineAdsResponse>>

}
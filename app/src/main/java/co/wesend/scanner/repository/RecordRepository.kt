package co.wesend.scanner.repository

import co.wesend.scanner.AppExecutors
import co.wesend.scanner.db.AppDatabase
import co.wesend.scanner.db.dao.RecordDao
import javax.inject.Inject
import javax.inject.Singleton
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.wesend.scanner.db.entity.Record
import co.wesend.scanner.vo.Resource


@Singleton
class RecordRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val db: AppDatabase,
    private val recordDao: RecordDao
) {

    fun getAllRecords(): LiveData<List<Record>> {
        return recordDao.findAll()
    }

    fun registerNewPrice(rate: Double, dateTime: Long, numOutliers: Int): Record {
        val record = Record(rate=rate, dateTime = dateTime, numOutliers = numOutliers)

        appExecutors.diskIO().execute {
            recordDao.insert(record = record)
        }

        return record
    }
}
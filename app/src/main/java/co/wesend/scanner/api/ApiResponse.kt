package co.wesend.scanner.api

import retrofit2.Response
import timber.log.Timber
import java.util.regex.Pattern


/**
 * Common class used by API responses.
 * @param <T> the type of the response object
</T> */
@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(error.message ?: "unknown error")
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(
                        body = body,
                        pag = if (body is BitcoinOnlineAdsResponse)
                            (body as BitcoinOnlineAdsResponse).pagination else null
                    )
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }
                ApiErrorResponse(errorMsg ?: "unknown error")
            }
        }
    }
}

class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorMessage: String) : ApiResponse<T>()

data class ApiSuccessResponse<T>(
    val body: T,
    val links: Map<String, Int>
) : ApiResponse<T>() {
    constructor(body: T, pag: Pagination?) : this(
        body = body,
        links = pag?.extractPages() ?: emptyMap()
    )

    val nextPage: Int? by lazy(LazyThreadSafetyMode.NONE) {
        links[NEXT_LINK]
    }

    val prevPage: Int? by lazy(LazyThreadSafetyMode.NONE) {
        links[PREV_LINK]
    }

    companion object {
        private const val NEXT_LINK = "next"
        private const val PREV_LINK = "prev"
        private val PAGE_PATTERN = Pattern.compile("\\bpage=(\\d+)")

        private fun Pagination.extractPages(): Map<String, Int> {
            val links = mutableMapOf<String, Int>()
            val matcherNext = PAGE_PATTERN.matcher(this.next)
            val matcherPrev = PAGE_PATTERN.matcher(this.prev)

            if (matcherNext.find() && matcherNext.groupCount() == 1) {
                links[NEXT_LINK] = Integer.parseInt(matcherNext.group(1))
            }

            if (matcherPrev.find() && matcherPrev.groupCount() == 1) {
                links[PREV_LINK] = Integer.parseInt(matcherNext.group(1))
            }

            return links
        }
    }
}
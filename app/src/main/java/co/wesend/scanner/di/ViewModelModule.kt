package co.wesend.scanner.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.wesend.scanner.ui.result.ResultViewModel
import co.wesend.scanner.viewmodel.RecordListViewModel
import co.wesend.scanner.viewmodel.ScannerViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RecordListViewModel::class)
    abstract fun bindRecordListViewModel(recordListViewModel: RecordListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ResultViewModel::class)
    abstract fun bindResultViewModel(resultViewModel: ResultViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(factory: ScannerViewModelFactory): ViewModelProvider.Factory
}